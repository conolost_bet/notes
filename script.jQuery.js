let noteList = [];
let container = $("#out").get(0);

$(document).ready(() => {
  
  if (localStorage.getItem("note") != undefined) {
    noteList = JSON.parse(localStorage.getItem("note"));
    out();
  }

  $("#add").click(() => {
    let d = $("#in").val();
    if (d) {
      let temp = {};
      temp.note = d;
      noteList.push(temp);
      save();
      $("#in").val("");
    }
  });
  enableTab($("#in").get(0));

  $("#sort").click(() => {
    if (2 <= noteList.length) {
      noteList.sort(function(a, b) {
        var noteA = a.note.toLowerCase(),
          noteB = b.note.toLowerCase();
        if (noteA < noteB) return -1;
        if (noteA < noteB) return 1;
        return 0;
      });
      save();
    }
  });
});

function createNote(text, id) {
  let newNote = $("<div />");
  $(newNote).attr("id", id);
  $("<pre />")
    .text(text)
    .appendTo(newNote);
  let cntrls = $("<div />").appendTo(newNote);

  $("<button />")
    .text("Delete")
    .click(delNote)
    .appendTo(cntrls);

  $("<button />")
    .text("Edit")
    .click(editNote)
    .appendTo(cntrls);
  return newNote;
}

function out() {
  container.innerHTML = "";
  for (let i = 0; i < noteList.length; i++) {
    $(createNote(noteList[i].note, i)).appendTo(container);
  }
}

function delNote(e) {
  let note = $(e.currentTarget)
    .parent()
    .parent()
    .attr("id");
  noteList.splice(+note, 1);
  save();
}

function editNote(e) {
  let note = $(e.currentTarget)
    .parent()
    .parent();
  let popUp = $(
    "<div class='b-popup'><div class='b-popup-content'><textarea></textarea><button>Save</button><button>Cancel</button></div></div>"
  ).appendTo(note);
  $(".b-popup-content")
    .children()
    .eq(1)
    .click(saveNote);
  $(".b-popup-content")
    .children()
    .eq(2)
    .click(cancel);

  $(".b-popup-content textarea").val(
    $(note)
      .children()
      .first()
      .text()
  );
  enableTab($(".b-popup-content textarea").get(0));
}

function saveNote(e) {
  let note = $(e.currentTarget)
    .parent()
    .parent()
    .parent();
  noteList[$(note).attr("id")].note = $(".b-popup-content textarea").val();
  save();
}

function cancel(e) {
  let note = $(e.currentTarget)
    .parent()
    .parent();
  $(note).remove();
}

function save() {
  window.localStorage.setItem("note", JSON.stringify(noteList));
  out();
}

function enableTab(el) {
  el.onkeydown = function(e) {
    if (e.keyCode === 9) {
      var val = this.value,
        start = this.selectionStart,
        end = this.selectionEnd;
      this.value = val.substring(0, start) + "\t" + val.substring(end);
      this.selectionStart = this.selectionEnd = start + 1;
      return false;
    }
  };
}
