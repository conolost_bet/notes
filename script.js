let noteList = [];
let container = document.querySelector("#out");

window.onload = function() {
  if (localStorage.getItem("note") != undefined) {
    noteList = JSON.parse(localStorage.getItem("note"));
    out();
  }

  document.getElementById("add").onclick = e => {
    let d = document.getElementById("in").value;
    let temp = {};
    temp.note = d;
    noteList.push(temp);
    save();
    document.getElementById("in").value = "";
  };
  enableTab(document.querySelector("#in"));
};

document.getElementById("sort").onclick = e => {
  let note = e.srcElement.parentNode.parentNode;
  if (2 <= noteList.length) {
    noteList.sort(function(a, b) {
      var noteA = a.note.toLowerCase(),
        noteB = b.note.toLowerCase();
      if (noteA < noteB)
        //сортируем строки по возрастанию
        return -1;
      if (noteA < noteB) return 1;
      return 0; // Никакой сортировки
    });
    save();
  }
};
function createNote(text, id) {
  let newNote = document.createElement("div");
  newNote.id = id;
  let txt = document.createElement("pre");
  let cntrls = document.createElement("div");
  txt.textContent = text;

  let delBttn = document.createElement("button");
  let editBttn = document.createElement("button");
  delBttn.textContent = "Delete";
  editBttn.textContent = "Edit";
  delBttn.addEventListener("click", delNote);
  editBttn.addEventListener("click", editNote);
  cntrls.appendChild(delBttn);
  cntrls.appendChild(editBttn);

  newNote.appendChild(txt);
  newNote.appendChild(cntrls);
  return newNote;
}

function out() {
  container.innerHTML = "";
  for (let i = 0; i < noteList.length; i++) {
    let note = createNote(noteList[i].note, i);
    container.appendChild(note);
  }
}

function delNote(e) {
  let note = e.srcElement.parentNode.parentNode;
  noteList.splice(+note.id, 1);
  save();
}

function editNote(e) {
  let note = e.srcElement.parentNode.parentNode;
  let popUp = document.createElement("div");
  let pop = document.createElement("div");
  popUp.appendChild(pop);
  popUp.classList.add("b-popup");
  pop.classList.add("b-popup-content");

  pop.appendChild(document.createElement("textarea"));
  pop.appendChild(document.createElement("button"));
  pop.appendChild(document.createElement("button"));
  pop.firstChild.nextSibling.textContent = "Save";
  pop.firstChild.nextSibling.addEventListener("click", saveNote);
  pop.lastChild.textContent = "Cancel";
  pop.lastChild.addEventListener("click", cancel);
  pop.firstChild.value = note.firstChild.textContent;
  note.appendChild(popUp);
  enableTab(pop.firstChild);
}

function saveNote(e) {
  let note = e.srcElement.parentNode.parentNode.parentNode;
  noteList[+note.id].note = e.srcElement.previousElementSibling.value;
  note.removeChild(note.lastChild);
  save();
}

function cancel(e) {
  let note = e.srcElement.parentNode.parentNode.parentNode;
  note.removeChild(note.lastChild);
}

function save() {
  window.localStorage.setItem("note", JSON.stringify(noteList));
  out();
}

function enableTab(el) {
  el.onkeydown = function(e) {
    if (e.keyCode === 9) {
      // tab was pressed

      // get caret position/selection
      var val = this.value,
        start = this.selectionStart,
        end = this.selectionEnd;

      // set textarea value to: text before caret + tab + text after caret
      this.value = val.substring(0, start) + "\t" + val.substring(end);

      // put caret at right position again
      this.selectionStart = this.selectionEnd = start + 1;

      // prevent the focus lose
      return false;
    }
  };
}
